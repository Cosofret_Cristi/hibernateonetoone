package com.sda.hibernate.runner;

import com.sda.hibernate.address.Address;
import com.sda.hibernate.user.User;
import com.sda.hibernate.util.HibernateUtil;
import org.hibernate.Session;

public class ApplicationRunner {

    public static void main(String[] args) {
        System.out.println("Hibernate one to one (Annotation)");
        Session session = HibernateUtil.getSessionFactory().openSession();

        session.beginTransaction();

        User user = new User();
        user.setName("Cristi");

        Address address = new Address();
        address.setStreet("Tudor Vladimirescu");

        user.setAddress(address);
        address.setUser(user);

        session.save(user);
        session.getTransaction().commit();

        System.out.println("Done");
    }

}
